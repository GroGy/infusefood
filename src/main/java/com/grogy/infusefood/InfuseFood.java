package com.grogy.infusefood;

import com.grogy.infusefood.registry.BlockRegistry;
import com.grogy.infusefood.registry.ClientRegistry;
import com.grogy.infusefood.registry.ItemRegistry;
import com.grogy.infusefood.registry.Registry;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.Collectors;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(Ref.Mod.ID)
public class InfuseFood
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();
    public static IEventBus MOD_EVENT_BUS;

    public InfuseFood() {
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);


        MOD_EVENT_BUS = FMLJavaModLoadingContext.get().getModEventBus();

        setup();
        DistExecutor.runWhenOn(Dist.CLIENT, () -> InfuseFood::clientSetup);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup()
    {
        MOD_EVENT_BUS.register(Registry.class);
        MOD_EVENT_BUS.register(ItemRegistry.class);
        MOD_EVENT_BUS.register(BlockRegistry.class);
    }

    private static void clientSetup() {
        MOD_EVENT_BUS.register(ClientRegistry.class);
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        clientSetup();
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            LOGGER.info("HELLO from Register Block");
        }
    }
}
