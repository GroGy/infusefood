package com.grogy.infusefood.block;

import com.grogy.infusefood.helper.SetBlockStateFlag;
import com.grogy.infusefood.tile.TileEssenceBuffer;
import com.mojang.brigadier.Message;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import java.text.MessageFormat;

public class BlockEssenceBuffer extends Block {
    public BlockEssenceBuffer() {
        super(Block.Properties.create(Material.ROCK).setRequiresTool().hardnessAndResistance(2.0f, 6.0f));
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new TileEssenceBuffer();
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos,
                                             PlayerEntity player, Hand handIn, BlockRayTraceResult blockRayTraceResult) {
        worldIn.notifyBlockUpdate(pos,state,state, SetBlockStateFlag.UPDATE_FLAGS);
        if (worldIn.getTileEntity(pos) instanceof TileEssenceBuffer && player.world.isRemote) {
            TileEssenceBuffer tile = (TileEssenceBuffer) worldIn.getTileEntity(pos);
            assert tile != null;
            player.sendStatusMessage(new StringTextComponent(MessageFormat.format("{0} / {1}", tile.getEssenceStored(),tile.getMaxEssence())),false);
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.SUCCESS;
    }
}
