package com.grogy.infusefood.block;

import com.grogy.infusefood.essence.IEssenceNode;
import com.grogy.infusefood.essence.IEssenceStorage;
import com.grogy.infusefood.tile.TileEssenceNode;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.PushReaction;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;


public class BlockEssenceNode extends SixWayBlock {
    public BlockEssenceNode() {
        super(0.1875f, AbstractBlock.Properties.create(Material.ROCK).notSolid().hardnessAndResistance(2.0f,6.0f));
        this.setDefaultState(this.getStateContainer().getBaseState().with(NORTH, Boolean.FALSE).with(EAST, Boolean.FALSE).with(SOUTH, Boolean.FALSE).with(WEST, Boolean.FALSE).with(UP, Boolean.FALSE).with(DOWN, Boolean.FALSE));
    }

    @Override
    public BlockRenderType getRenderType(BlockState iBlockState) {
        return BlockRenderType.MODEL;
    }

    @Override
    public boolean getWeakChanges(BlockState state, IWorldReader world, BlockPos pos) {
        return false;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(NORTH, EAST, WEST, SOUTH, UP, DOWN);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {

        TileEntity tileDown = worldIn.getTileEntity(pos.add(Direction.DOWN.getDirectionVec()));
        TileEntity tileUp = worldIn.getTileEntity(pos.add(Direction.UP.getDirectionVec()));
        TileEntity tileNorth = worldIn.getTileEntity(pos.add(Direction.NORTH.getDirectionVec()));
        TileEntity tileSouth = worldIn.getTileEntity(pos.add(Direction.SOUTH.getDirectionVec()));
        TileEntity tileEast = worldIn.getTileEntity(pos.add(Direction.EAST.getDirectionVec()));
        TileEntity tileWest = worldIn.getTileEntity(pos.add(Direction.WEST.getDirectionVec()));

        BlockState result = state.with(FACING_TO_PROPERTY_MAP.get(Direction.DOWN), tileDown instanceof IEssenceStorage && !(tileDown instanceof IEssenceNode))
                .with(FACING_TO_PROPERTY_MAP.get(Direction.UP), tileUp instanceof IEssenceStorage && !(tileUp instanceof IEssenceNode))
                .with(FACING_TO_PROPERTY_MAP.get(Direction.NORTH), tileNorth instanceof IEssenceStorage && !(tileNorth instanceof IEssenceNode))
                .with(FACING_TO_PROPERTY_MAP.get(Direction.SOUTH), tileSouth instanceof IEssenceStorage && !(tileSouth instanceof IEssenceNode))
                .with(FACING_TO_PROPERTY_MAP.get(Direction.EAST), tileEast instanceof IEssenceStorage && !(tileEast instanceof IEssenceNode))
                .with(FACING_TO_PROPERTY_MAP.get(Direction.WEST), tileWest instanceof IEssenceStorage && !(tileWest instanceof IEssenceNode));
        worldIn.setBlockState(pos, result);
    }

    @Override
    public BlockState updatePostPlacement(BlockState thisBlockState, Direction directionFromThisToNeighbor, BlockState neighborState,
                                          IWorld worldIn, BlockPos thisBlockPos, BlockPos neighborBlockPos) {
        TileEntity tile = worldIn.getTileEntity(neighborBlockPos);
        return thisBlockState.with(FACING_TO_PROPERTY_MAP.get(directionFromThisToNeighbor), tile instanceof IEssenceStorage && !(tile instanceof IEssenceNode));
    }


    @Override
    public boolean allowsMovement(BlockState state, IBlockReader worldIn, BlockPos pos, PathType type) {
        return false;
    }

    @Override
    public PushReaction getPushReaction(BlockState state) {
        return PushReaction.DESTROY;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new TileEssenceNode();
    }
}
