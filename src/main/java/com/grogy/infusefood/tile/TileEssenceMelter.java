package com.grogy.infusefood.tile;

import com.grogy.infusefood.Ref;
import com.grogy.infusefood.container.ContainerEssenceMelter;
import com.grogy.infusefood.container.inventory.InventoryEssenceMelter;
import com.grogy.infusefood.container.states.ContainerStateEssenceMelter;
import com.grogy.infusefood.essence.IEssenceStorage;
import com.grogy.infusefood.helper.SetBlockStateFlag;
import com.grogy.infusefood.registry.BlockRegistry;
import com.grogy.infusefood.registry.ItemRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class TileEssenceMelter extends TileEntity implements INamedContainerProvider,ITickableTileEntity, IEssenceStorage {
    private InventoryEssenceMelter inventoryEssenceMelter;

    private final ContainerStateEssenceMelter essenceMelterState = new ContainerStateEssenceMelter();

    private final int ESSENCE_PER_TICK = 32;
    private final int ESSENCE_MAX = 16000;
    private final int ESSENCE_MAX_EXTRACTED = 64;

    public TileEssenceMelter() {
        super(BlockRegistry.tileEssenceMelter);
        inventoryEssenceMelter = InventoryEssenceMelter.createForTileEntity(
                this::canPlayerAccessInventory, this::markDirty);
    }

    public boolean canPlayerAccessInventory(PlayerEntity player) {
        if (this.world.getTileEntity(this.pos) != this) return false;
        final double X_CENTRE_OFFSET = 0.5;
        final double Y_CENTRE_OFFSET = 0.5;
        final double Z_CENTRE_OFFSET = 0.5;
        final double MAXIMUM_DISTANCE_SQ = 8.0 * 8.0;
        return player.getDistanceSq(pos.getX() + X_CENTRE_OFFSET, pos.getY() + Y_CENTRE_OFFSET, pos.getZ() + Z_CENTRE_OFFSET) < MAXIMUM_DISTANCE_SQ;
    }

    @Override
    public void tick() {
        if (!this.hasWorld()) return;
        World world = this.getWorld();
        if (world.isRemote) // client
        {
            if(essenceMelterState.working == 1) {
                world.addParticle(ParticleTypes.ITEM_SLIME, getPos().getX() + 0.5, getPos().getY() + 0.5, getPos().getZ() + 0.5, 0, 0.05f, 0);
            }
        }
        else // server
        {
            if(essenceMelterState.working == 1) {
                if(essenceMelterState.remainingEssence < ESSENCE_PER_TICK) {
                    int remain = insertEssence(essenceMelterState.remainingEssence);
                    essenceMelterState.working = 0;
                    essenceMelterState.totalEssence = 0;
                    BlockState currentBlockState = world.getBlockState(this.pos);
                    final int FLAGS = SetBlockStateFlag.get(SetBlockStateFlag.BLOCK_UPDATE, SetBlockStateFlag.SEND_TO_CLIENTS);
                    world.notifyBlockUpdate(this.pos,currentBlockState,currentBlockState,FLAGS);
                } else {
                    int remain = insertEssence(ESSENCE_PER_TICK);
                    essenceMelterState.remainingEssence -= ESSENCE_PER_TICK;
                }
                markDirty();
            }

            if(inventoryEssenceMelter.getStackInSlot(0).getItem().isFood() && essenceMelterState.working == 0) {
                essenceMelterState.working = 1;
                essenceMelterState.totalEssence = inventoryEssenceMelter.getStackInSlot(0).getItem().getFood().getHealing()*200;
                inventoryEssenceMelter.decrStackSize(0,1);
                essenceMelterState.remainingEssence = essenceMelterState.totalEssence;
                markDirty();
                BlockState currentBlockState = world.getBlockState(this.pos);
                world.notifyBlockUpdate(this.pos,currentBlockState,currentBlockState,SetBlockStateFlag.UPDATE_FLAGS);
            }
        }
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        CompoundNBT updateTagDescribingTileEntityState = getUpdateTag();
        final int METADATA = 42; // arbitrary.
        return new SUpdateTileEntityPacket(this.pos, METADATA, updateTagDescribingTileEntityState);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        CompoundNBT updateTagDescribingTileEntityState = pkt.getNbtCompound();
        BlockState blockState = world.getBlockState(pos);
        handleUpdateTag(blockState, updateTagDescribingTileEntityState);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT nbtTagCompound = new CompoundNBT();
        write(nbtTagCompound);
        return nbtTagCompound;
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        read(state, tag);
    }

    private final String INVENTORY_NBT = "buffer";

    @Override
    public void read(BlockState blockState, CompoundNBT parentNBTTagCompound) {
        super.read(blockState, parentNBTTagCompound); // The super call is required to save and load the tile's location
        essenceMelterState.readFromNBT(parentNBTTagCompound);
        essenceStorage = readEssence(parentNBTTagCompound);
        CompoundNBT inventoryNBT = parentNBTTagCompound.getCompound(INVENTORY_NBT);
        inventoryEssenceMelter.deserializeNBT(inventoryNBT);

        if (inventoryEssenceMelter.getSizeInventory() != 1)
            throw new IllegalArgumentException("Corrupted NBT: Number of inventory slots did not match expected.");
    }

    @Override
    public CompoundNBT write(CompoundNBT parentNBTTagCompound)
    {
        super.write(parentNBTTagCompound); // The super call is required to save and load the tile's location
        essenceMelterState.putIntoNBT(parentNBTTagCompound);
        writeEssence(parentNBTTagCompound);
        parentNBTTagCompound.put(INVENTORY_NBT,inventoryEssenceMelter.serializeNBT());
        return parentNBTTagCompound;
    }

    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent(Ref.Container.ESSENCE_MELTER);
    }

    @Override
    public Container createMenu(int windowID, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return ContainerEssenceMelter.createContainerServerSide(windowID, playerInventory,
                inventoryEssenceMelter, essenceMelterState);
    }

    public void dropAllContents(World world, BlockPos blockPos) {
        InventoryHelper.dropInventoryItems(world, blockPos,inventoryEssenceMelter);
    }

    @Override
    public int getMaxEssence() {
        return ESSENCE_MAX;
    }

    @Override
    public int getMaxEssenceReceived() {
        return 0;
    }

    @Override
    public int getMaxEssenceExtracted() {
        return ESSENCE_MAX_EXTRACTED;
    }

    @Override
    public int getEssenceStored() {
        return essenceStorage;
    }

    @Override
    public void setEssenceStored(int essence) {
        essenceStorage = essence;
    }

    private int essenceStorage = 0;
}
