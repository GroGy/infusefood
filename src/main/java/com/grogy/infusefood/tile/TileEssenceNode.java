package com.grogy.infusefood.tile;

import com.grogy.infusefood.essence.IEssenceNode;
import com.grogy.infusefood.essence.IEssenceStorage;
import com.grogy.infusefood.helper.SetBlockStateFlag;
import com.grogy.infusefood.registry.BlockRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.SixWayBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.particles.RedstoneParticleData;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.Map;

public class TileEssenceNode extends TileEntity implements ITickableTileEntity, IEssenceNode {
    private int transferSpeed = 128;

    private final int PARTICLE_FREQUENCY = 10;
    private int particleCounter = 0;
    private boolean oddParticle = false;

    private boolean connected = false;
    private BlockPos otherConnection = BlockPos.ZERO;

    private final String OTHER_CONNECTION_NBT = "otherNode";
    private final String IS_CONNECTED_NBT = "connected";

    public TileEssenceNode() {
        super(BlockRegistry.tileEssenceNode);
    }

    public void setTransferSpeed(int val) {
        transferSpeed = val;
    }

    @Override
    public void tick() {
        if (connected && world.isRemote) {
            particleCounter++;
            if (particleCounter > PARTICLE_FREQUENCY) {
                particleCounter = 0;
                oddParticle = !oddParticle;
                if(oddParticle) {
                    world.addParticle(new RedstoneParticleData(1.0f, 0, 0, 1.0f), pos.getX() + 0.8, pos.getY() + 0.8, pos.getZ() + 0.8, 0.0, 0.1, 0.0);
                    world.addParticle(new RedstoneParticleData(1.0f, 0, 0, 1.0f), pos.getX() + 0.2, pos.getY() + 0.2, pos.getZ() + 0.2, 0.0, 0.1, 0.0);
                } else {
                    world.addParticle(new RedstoneParticleData(1.0f, 0, 0, 1.0f), pos.getX() + 0.8, pos.getY() + 0.2, pos.getZ() + 0.8, 0.0, 0.1, 0.0);
                    world.addParticle(new RedstoneParticleData(1.0f, 0, 0, 1.0f), pos.getX() + 0.2, pos.getY() + 0.8, pos.getZ() + 0.2, 0.0, 0.1, 0.0);
                }
            }
        }

        if(!world.isRemote) {

            Map<Direction,Boolean> insertedTo = new HashMap<Direction, Boolean>() {{
                put(Direction.UP, false);
                put(Direction.DOWN, false);
                put(Direction.EAST, false);
                put(Direction.NORTH, false);
                put(Direction.WEST, false);
                put(Direction.SOUTH, false);
            }};

            int toInsert = extractEssence(transferSpeed);

            for (int i = 0; i < Direction.values().length; i++) {
                if (toInsert == 0) break;
                if (getBlockState().get(SixWayBlock.FACING_TO_PROPERTY_MAP.get(Direction.values()[i])) == Boolean.TRUE) {
                    TileEntity tile = world.getTileEntity(getPos().add(Direction.values()[i].getDirectionVec()));
                    if (tile instanceof IEssenceStorage) {
                        int maxReceived = ((IEssenceStorage) tile).getMaxEssenceReceived();
                        int amountToInsert = Math.min(maxReceived, toInsert);
                        if (amountToInsert == 0) continue;
                        toInsert -= amountToInsert;
                        toInsert += ((IEssenceStorage) tile).insertEssence(amountToInsert);
                        insertedTo.put(Direction.values()[i],true);
                    }
                }
            }

            if(toInsert > 0) {
                insertEssence(toInsert);
            }

            for (int i = 0; i < Direction.values().length; i++) {
                if (getEssenceStored() == getMaxEssence()) break;
                if (getBlockState().get(SixWayBlock.FACING_TO_PROPERTY_MAP.get(Direction.values()[i])) == Boolean.TRUE) {
                    if(insertedTo.get(Direction.values()[i])) continue;
                    TileEntity tile = world.getTileEntity(getPos().add(Direction.values()[i].getDirectionVec()));
                    if (tile instanceof IEssenceStorage) {
                        int maxExtracted = ((IEssenceStorage) tile).getMaxEssenceExtracted();
                        int amountToExtract = Math.min(maxExtracted, getMaxEssence() - getEssenceStored());
                        if (amountToExtract == 0) continue;
                        int extracted = ((IEssenceStorage) tile).extractEssence(amountToExtract);
                        ((IEssenceStorage) tile).insertEssence(insertEssence(extracted));
                    }
                }
            }

            if (connected) {
                TileEntity other = world.getTileEntity(otherConnection);
                if(other instanceof TileEssenceNode) {
                    if(((TileEssenceNode) other).getEssenceStored() != ((TileEssenceNode) other).getMaxEssence()) {
                        int amount = this.extractEssence(getMaxEssenceExtracted());
                        insertEssence(((TileEssenceNode) other).insertEssence(amount));
                    }
                }
            }
        }
    }

    @Override
    public int getMaxEssence() {
        return transferSpeed;
    }

    @Override
    public int getMaxEssenceReceived() {
        return transferSpeed;
    }

    @Override
    public int getMaxEssenceExtracted() {
        return transferSpeed;
    }

    @Override
    public int getEssenceStored() {
        return essenceStorage;
    }

    @Override
    public void setEssenceStored(int essence) {
        essenceStorage = essence;
    }

    @Override
    public boolean canSend() {
        return true;
    }

    private int essenceStorage = 0;

    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        super.read(state, nbt);
        readEssence(nbt);
        otherConnection = BlockPos.fromLong(nbt.getLong(OTHER_CONNECTION_NBT));
        connected = nbt.getBoolean(IS_CONNECTED_NBT);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putLong(OTHER_CONNECTION_NBT, otherConnection.toLong());
        compound.putBoolean(IS_CONNECTED_NBT, connected);
        return writeEssence(compound);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        CompoundNBT updateTagDescribingTileEntityState = getUpdateTag();
        final int METADATA = 42; // arbitrary.
        return new SUpdateTileEntityPacket(this.pos, METADATA, updateTagDescribingTileEntityState);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT nbtTagCompound = new CompoundNBT();
        write(nbtTagCompound);
        return nbtTagCompound;
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        read(state, tag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        CompoundNBT updateTagDescribingTileEntityState = pkt.getNbtCompound();
        BlockState blockState = world.getBlockState(pos);
        handleUpdateTag(blockState, updateTagDescribingTileEntityState);
    }

    public void unlinkConnections() {
        if (!connected) return;
        if (world.isRemote) return;
        connected = false;
        TileEntity tile = world.getTileEntity(otherConnection);
        if(tile instanceof TileEssenceNode) {
            ((TileEssenceNode) tile).otherConnection = BlockPos.ZERO;
            ((TileEssenceNode) tile).connected = false;
        }

        final int FLAGS = SetBlockStateFlag.get(SetBlockStateFlag.BLOCK_UPDATE, SetBlockStateFlag.SEND_TO_CLIENTS);
        world.notifyBlockUpdate(getPos(),getBlockState(),getBlockState(),FLAGS);
        world.notifyBlockUpdate(otherConnection,world.getBlockState(otherConnection),world.getBlockState(otherConnection),FLAGS);
        otherConnection = BlockPos.ZERO;
    }

    public void linkWithOther(TileEssenceNode other) {
        otherConnection = other.getPos();
        other.otherConnection = getPos();
        connected = true;
        other.connected = true;

        final int FLAGS = SetBlockStateFlag.get(SetBlockStateFlag.BLOCK_UPDATE, SetBlockStateFlag.SEND_TO_CLIENTS);

        world.notifyBlockUpdate(getPos(),getBlockState(),getBlockState(),FLAGS);
        world.notifyBlockUpdate(otherConnection,world.getBlockState(otherConnection),world.getBlockState(otherConnection),FLAGS);
    }}
