package com.grogy.infusefood.tile;

import com.grogy.infusefood.essence.IEssenceStorage;
import com.grogy.infusefood.helper.SetBlockStateFlag;
import com.grogy.infusefood.registry.BlockRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

public class TileEssenceBuffer extends TileEntity implements ITickableTileEntity, IEssenceStorage {
    public TileEssenceBuffer() {
        super(BlockRegistry.tileEssenceBuffer);
    }

    private final int MAX_STORED = 256000;
    private final int MAX_PER_SECOND = 256;
    private int stored = 0;

    @Override
    public int getMaxEssence() {
        return MAX_STORED;
    }

    @Override
    public int getMaxEssenceReceived() {
        return MAX_PER_SECOND;
    }

    @Override
    public int getMaxEssenceExtracted() {
        return MAX_PER_SECOND;
    }

    @Override
    public int getEssenceStored() {
        return stored;
    }

    @Override
    public void setEssenceStored(int essence) {
        stored = essence;
    }

    @Override
    public void tick() {

    }

    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        super.read(state, nbt);
        setEssenceStored(readEssence(nbt));
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        return writeEssence(compound);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        CompoundNBT updateTagDescribingTileEntityState = getUpdateTag();
        final int METADATA = 42; // arbitrary.
        return new SUpdateTileEntityPacket(this.pos, METADATA, updateTagDescribingTileEntityState);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT nbtTagCompound = new CompoundNBT();
        write(nbtTagCompound);
        return nbtTagCompound;
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        read(state, tag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        CompoundNBT updateTagDescribingTileEntityState = pkt.getNbtCompound();
        BlockState blockState = world.getBlockState(pos);
        handleUpdateTag(blockState, updateTagDescribingTileEntityState);
    }


}
