package com.grogy.infusefood;

import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;

public class Ref {
    public static class Mod
    {
        public static final String ID         = "infusefood";
        public static final String NAME       = "InfuseFood";
        public static final String VERSION    = "${version}";
        public static final String MC_VERSION = "1.16.4";
        public static final String DEPS       = "";
    }

    public static class Blocks {
        public static final String FLYING_FAKE_STEAK = "flying_fake_steak";
        public static final String ESSENCE_MELTER = "essence_melter";
        public static final String ESSENCE_NODE = "essence_node";
        public static final String ESSENCE_BUFFER = "essence_buffer";
        //TODO: Fix tiers

    }
    public static class Tile {
        public static final String ESSENCE_MELTER = "tile_entity_essence_melter";
        public static final String ESSENCE_NODE = "tile_entity_essence_node";
        public static final String ESSENCE_BUFFER = "tile_entity_essence_buffer";
    }

    public static class Gui {
        public static final ResourceLocation ESSENCE_MELTER = new ResourceLocation(Mod.ID,"textures/gui/essence_melter.png");
    }

    public static class Items {
        public static final String OVERCOOKED_STEAK = "overcooked_steak";
        public static final String FLUXED_STEAK = "fluxed_steak";
        public static final String DEBUG_STICK = "debug_stick";
    }

    public static class ToolTips {
        public static final String MAGICAL_BEEF_STICK_TOOLTIP = "Oh shiny!";
    }

    public static class Proxy
    {
        public static final String PROXY_CLIENT_CLASS = "com.grogy.infusefood.proxy.ClientProxy";
        public static final String PROXY_SERVER_CLASS = "com.grogy.infusefood.proxy.ServerProxy";
    }

    public static class Network
    {
        public static final String NETWORK_CHANNEL = Mod.ID;

        public static class Packet
        {

        }
    }


    public static class Command
    {
        public static final String FOOD        = "food";
        public static final String FOOD_POTION = "foodpotion";
    }

    public static class Expression
    {
        public static final char PARAGRAPH = '\u00a7';
    }

    public static class Inventory
    {
        public static ItemGroup CREATIVE_TAB;
    }

    public static class Container {
        public static String ESSENCE_MELTER = "container_essence_melter";
    }
}
