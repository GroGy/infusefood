package com.grogy.infusefood.registry;

import com.grogy.infusefood.container.screens.ContainerScreenEssenceMelter;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

public class ClientRegistry {
    @SubscribeEvent
    public static void onClientSetupEvent(FMLClientSetupEvent event) {
        ScreenManager.registerFactory(BlockRegistry.containerTypeEssenceMelter, ContainerScreenEssenceMelter::new);
    }
}
