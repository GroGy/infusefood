package com.grogy.infusefood.registry;

import com.grogy.infusefood.Ref;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class Registry {
    @SubscribeEvent
    public static void onItemsRegistration(final RegistryEvent.Register<Item> itemRegisterEvent) {
        Ref.Inventory.CREATIVE_TAB = new ItemGroup(Ref.Mod.ID) {
            @Override
            public ItemStack createIcon() {
                return new ItemStack(Items.CARVED_PUMPKIN);
            }
        };
    }
}
