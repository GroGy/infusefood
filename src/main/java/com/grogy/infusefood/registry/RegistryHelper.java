package com.grogy.infusefood.registry;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;

public class RegistryHelper {
    public static <T extends Item> void registerItem(final RegistryEvent.Register<Item> itemRegisterEvent, T item, String registryName) {
        item.setRegistryName(registryName);
        itemRegisterEvent.getRegistry().register(item);
    }

    public static <T extends Block> void registerBlock(final RegistryEvent.Register<Block> blockRegisterEvent, T block, String registryName) {
        block.setRegistryName(registryName);
        blockRegisterEvent.getRegistry().register(block);
    }

    public static <T extends BlockItem> void registerBlockItem(final RegistryEvent.Register<Item> blockRegisterEvent, T blockItem, ResourceLocation registryName) {
        blockItem.setRegistryName(registryName);
        blockRegisterEvent.getRegistry().register(blockItem);
    }

    public static <T extends TileEntityType<?>> void registerTileEntity(final RegistryEvent.Register<TileEntityType<?>> event, T tile, String registryName) {
        tile.setRegistryName(registryName);
        event.getRegistry().register(tile);
    }
}
