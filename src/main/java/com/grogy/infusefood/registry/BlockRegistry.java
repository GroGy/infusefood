package com.grogy.infusefood.registry;

import com.grogy.infusefood.Ref;
import com.grogy.infusefood.block.BlockEssenceBuffer;
import com.grogy.infusefood.block.BlockEssenceMelter;
import com.grogy.infusefood.block.BlockEssenceNode;
import com.grogy.infusefood.block.BlockFlyingFakeSteak;
import com.grogy.infusefood.container.ContainerEssenceMelter;
import com.grogy.infusefood.tile.TileEssenceBuffer;
import com.grogy.infusefood.tile.TileEssenceMelter;
import com.grogy.infusefood.tile.TileEssenceNode;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import static com.grogy.infusefood.registry.RegistryHelper.*;

public class BlockRegistry {
    public static BlockItem flyingFakeSteakItem;
    public static BlockFlyingFakeSteak flyingFakeSteak;

    public static BlockItem essenceMelterItem;
    public static BlockEssenceMelter essenceMelter;
    public static TileEntityType<TileEssenceMelter> tileEssenceMelter;
    public static ContainerType<ContainerEssenceMelter> containerTypeEssenceMelter;

    public static BlockItem essenceNodeItem;
    public static BlockEssenceNode essenceNode;
    public static TileEntityType<TileEssenceNode> tileEssenceNode;

    public static BlockItem essenceBufferItem;
    public static BlockEssenceBuffer essenceBuffer;
    public static TileEntityType<TileEssenceBuffer> tileEssenceBuffer;

    @SubscribeEvent
    public static void onBlocksRegistration(final RegistryEvent.Register<Block> blockRegisterEvent) {
        flyingFakeSteak = new BlockFlyingFakeSteak();
        registerBlock(blockRegisterEvent,flyingFakeSteak,Ref.Blocks.FLYING_FAKE_STEAK);
        essenceMelter = new BlockEssenceMelter();
        registerBlock(blockRegisterEvent,essenceMelter,Ref.Blocks.ESSENCE_MELTER);
        essenceNode = new BlockEssenceNode();
        registerBlock(blockRegisterEvent,essenceNode,Ref.Blocks.ESSENCE_NODE);
        essenceBuffer = new BlockEssenceBuffer();
        registerBlock(blockRegisterEvent,essenceBuffer,Ref.Blocks.ESSENCE_BUFFER);
    }

    @SubscribeEvent
    public static void onItemsRegistration(final RegistryEvent.Register<Item> itemRegisterEvent) {
        flyingFakeSteakItem = new BlockItem(flyingFakeSteak, new Item.Properties());
        registerBlockItem(itemRegisterEvent,flyingFakeSteakItem,flyingFakeSteak.getRegistryName());

        essenceMelterItem = new BlockItem(essenceMelter, new Item.Properties().group(Ref.Inventory.CREATIVE_TAB));
        registerBlockItem(itemRegisterEvent,essenceMelterItem,essenceMelter.getRegistryName());

        essenceNodeItem = new BlockItem(essenceNode, new Item.Properties().group(Ref.Inventory.CREATIVE_TAB));
        registerBlockItem(itemRegisterEvent,essenceNodeItem,essenceNode.getRegistryName());

        essenceBufferItem = new BlockItem(essenceBuffer, new Item.Properties().group(Ref.Inventory.CREATIVE_TAB));
        registerBlockItem(itemRegisterEvent,essenceBufferItem,essenceBuffer.getRegistryName());
    }

    @SubscribeEvent
    public static void onTileEntityTypeRegistration(final RegistryEvent.Register<TileEntityType<?>> event) {
        tileEssenceMelter = TileEntityType.Builder.create(TileEssenceMelter::new, essenceMelter).build(null);  // you probably don't need a datafixer --> null should be fine
        registerTileEntity(event,tileEssenceMelter,Ref.Tile.ESSENCE_MELTER);

        tileEssenceNode = TileEntityType.Builder.create(TileEssenceNode::new, essenceNode).build(null);  // you probably don't need a datafixer --> null should be fine
        registerTileEntity(event,tileEssenceNode,Ref.Tile.ESSENCE_NODE);

        tileEssenceBuffer = TileEntityType.Builder.create(TileEssenceBuffer::new, essenceBuffer).build(null);  // you probably don't need a datafixer --> null should be fine
        registerTileEntity(event,tileEssenceBuffer,Ref.Tile.ESSENCE_BUFFER);
    }

    @SubscribeEvent
    public static void registerContainers(final RegistryEvent.Register<ContainerType<?>> event)
    {
        //TODO: Helper
        containerTypeEssenceMelter = IForgeContainerType.create(ContainerEssenceMelter::createContainerClientSide);
        containerTypeEssenceMelter.setRegistryName(Ref.Container.ESSENCE_MELTER);
        event.getRegistry().register(containerTypeEssenceMelter);
    }
}
