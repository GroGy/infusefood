package com.grogy.infusefood.registry;

import com.grogy.infusefood.Ref;
import com.grogy.infusefood.item.ItemEssenceStuff;
import net.minecraft.item.Item;
import net.minecraft.item.Rarity;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import static com.grogy.infusefood.registry.RegistryHelper.registerItem;

public class ItemRegistry {
    public static Item itemOvercookedSteak;
    public static Item itemFluxedSteak;
    public static ItemEssenceStuff itemDebugStick;

    @SubscribeEvent
    public static void onItemsRegistration(final RegistryEvent.Register<Item> itemRegisterEvent) {
        itemOvercookedSteak = new Item(new Item.Properties().group(Ref.Inventory.CREATIVE_TAB));
        registerItem(itemRegisterEvent, itemOvercookedSteak, Ref.Items.OVERCOOKED_STEAK);
        itemFluxedSteak = new Item(new Item.Properties().group(Ref.Inventory.CREATIVE_TAB));
        registerItem(itemRegisterEvent, itemFluxedSteak, Ref.Items.FLUXED_STEAK);
        itemDebugStick = new ItemEssenceStuff();
        registerItem(itemRegisterEvent, itemDebugStick, Ref.Items.DEBUG_STICK);
    }


    @SubscribeEvent
    public static void onCommonSetupEvent(FMLCommonSetupEvent event) {
    }

}
