package com.grogy.infusefood.essence;

public interface IEssenceNode extends IEssenceStorage {
    boolean canSend();
}
