package com.grogy.infusefood.essence;

import net.minecraft.nbt.CompoundNBT;

public interface IEssenceStorage {
    int getMaxEssence();
    int getMaxEssenceReceived();
    int getMaxEssenceExtracted();
    int getEssenceStored();
    void setEssenceStored(int essence);

    /// Returns left essence, if there is any overflow because we cant fit essence, it will be returned
    default int insertEssence(int essence){
        if(getEssenceStored() == getMaxEssence()) {
            return essence;
        }
        if(getEssenceStored() + essence > getMaxEssence()) {
            int res = getEssenceStored() + essence - getMaxEssence();
            setEssenceStored(getMaxEssence());
            return res;
        }

        setEssenceStored(getEssenceStored() + essence);
        return 0;
    }
    /// Returns extracted amount
    default int extractEssence(int essence){
        if(essence < 0 || getEssenceStored() == 0) {
            return 0;
        }

        if(getEssenceStored() < essence) {
            int res = getEssenceStored();
            setEssenceStored(0);
            return res;
        }

        setEssenceStored(getEssenceStored() - essence);
        return essence;
    }
    default int readEssence(CompoundNBT parentNBTTagCompound) {
        return parentNBTTagCompound.getInt("Essence");
    }

    default CompoundNBT writeEssence(CompoundNBT parentNBTTagCompound) {
        parentNBTTagCompound.putInt("Essence",getEssenceStored());
        return parentNBTTagCompound;
    }
}
