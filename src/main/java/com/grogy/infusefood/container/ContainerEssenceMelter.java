package com.grogy.infusefood.container;

import com.grogy.infusefood.container.inventory.InventoryEssenceMelter;
import com.grogy.infusefood.container.states.ContainerStateEssenceMelter;
import com.grogy.infusefood.registry.BlockRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class ContainerEssenceMelter extends Container {

    private static final int HOTBAR_SLOT_COUNT = 9;
    private static final int PLAYER_INVENTORY_ROW_COUNT = 3;
    private static final int PLAYER_INVENTORY_COLUMN_COUNT = 9;
    private static final int PLAYER_INVENTORY_SLOT_COUNT = PLAYER_INVENTORY_COLUMN_COUNT * PLAYER_INVENTORY_ROW_COUNT;
    private static final int VANILLA_SLOT_COUNT = HOTBAR_SLOT_COUNT + PLAYER_INVENTORY_SLOT_COUNT;

    public static final int ESSENCE_MELTER_SLOT_COUNT = 1;

    // slot index is the unique index for all slots in this container i.e. 0 - 35 for invPlayer then 36 - 49 for furnaceContents
    private static final int VANILLA_FIRST_SLOT_INDEX = 0;
    private static final int HOTBAR_FIRST_SLOT_INDEX = VANILLA_FIRST_SLOT_INDEX;
    private static final int PLAYER_INVENTORY_FIRST_SLOT_INDEX = HOTBAR_FIRST_SLOT_INDEX + HOTBAR_SLOT_COUNT;
    private static final int ESSENCE_MELTER_SLOT_INDEX = PLAYER_INVENTORY_FIRST_SLOT_INDEX + PLAYER_INVENTORY_SLOT_COUNT;

    // gui position of the player inventory grid
    public static final int PLAYER_INVENTORY_XPOS = 8;
    public static final int PLAYER_INVENTORY_YPOS = 67;

    protected ContainerEssenceMelter(int windowID, PlayerInventory invPlayer, InventoryEssenceMelter essenceMelterInventory, ContainerStateEssenceMelter essenceMelterState) {
        super(BlockRegistry.containerTypeEssenceMelter, windowID);

        this.world = invPlayer.player.world;
        this.essenceMelterStateData = essenceMelterState;
        this.essenceMelterInventory = essenceMelterInventory;
        trackIntArray(essenceMelterState);

        final int SLOT_X_SPACING = 18;
        final int SLOT_Y_SPACING = 18;
        final int HOTBAR_XPOS = 8;
        final int HOTBAR_YPOS = 125;
        // Add the players hotbar to the gui - the [xpos, ypos] location of each item
        for (int x = 0; x < HOTBAR_SLOT_COUNT; x++) {
            addSlot(new Slot(invPlayer, x, HOTBAR_XPOS + SLOT_X_SPACING * x, HOTBAR_YPOS));
        }

        // Add the rest of the players inventory to the gui
        for (int y = 0; y < PLAYER_INVENTORY_ROW_COUNT; y++) {
            for (int x = 0; x < PLAYER_INVENTORY_COLUMN_COUNT; x++) {
                int slotNumber = HOTBAR_SLOT_COUNT + y * PLAYER_INVENTORY_COLUMN_COUNT + x;
                int xpos = PLAYER_INVENTORY_XPOS + x * SLOT_X_SPACING;
                int ypos = PLAYER_INVENTORY_YPOS + y * SLOT_Y_SPACING;
                addSlot(new Slot(invPlayer, slotNumber,  xpos, ypos));
            }
        }

        addSlot(new Slot(essenceMelterInventory, 0,  80, 32));
    }

    public static ContainerEssenceMelter createContainerServerSide(int windowID, PlayerInventory playerInventory,
                                                                   InventoryEssenceMelter essenceMelterInventory,
                                                                   ContainerStateEssenceMelter essenceMelterState) {
        return new ContainerEssenceMelter(windowID, playerInventory, essenceMelterInventory, essenceMelterState);
    }

    public static ContainerEssenceMelter createContainerClientSide(int windowID, PlayerInventory playerInventory, net.minecraft.network.PacketBuffer extraData) {
        ContainerStateEssenceMelter essenceMelterState = new ContainerStateEssenceMelter();

        // on the client side there is no parent TileEntity to communicate with, so we:
        // 1) use dummy inventories and furnace state data (tracked ints)
        // 2) use "do nothing" lambda functions for canPlayerAccessInventory and markDirty
        return new ContainerEssenceMelter(windowID, playerInventory,InventoryEssenceMelter.createForClientSideContainer(), essenceMelterState);
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return essenceMelterInventory.isUsableByPlayer(playerIn);
    }


    public double fractionRemaining() {
        if (essenceMelterStateData.remainingEssence <= 0 ) return 0;
        double fraction = essenceMelterStateData.remainingEssence / (double)essenceMelterStateData.totalEssence;
        return MathHelper.clamp(fraction, 0.0, 1.0);
    }

    @Override
    protected boolean mergeItemStack(ItemStack stack, int startIndex, int endIndex, boolean reverseDirection) {
        return super.mergeItemStack(stack, startIndex, endIndex, reverseDirection);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {

        return ItemStack.EMPTY;
    }

    private World world;
    private ContainerStateEssenceMelter essenceMelterStateData;
    private InventoryEssenceMelter essenceMelterInventory;
}
