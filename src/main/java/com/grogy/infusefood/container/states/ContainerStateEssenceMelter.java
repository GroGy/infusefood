package com.grogy.infusefood.container.states;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntReferenceHolder;

public class ContainerStateEssenceMelter implements IIntArray {
    private final String STORED_ESSENCE_NBT = "StoredEssence";
    private final String TOTAL_ESSENCE_NBT = "TotalEssence";
    private final String WORKING_NBT = "Working";

    public int remainingEssence = 0;
    public int totalEssence = 0;
    public int working = 0;

    public void putIntoNBT(CompoundNBT nbtTagCompound) {
        nbtTagCompound.putInt(STORED_ESSENCE_NBT, remainingEssence);
        nbtTagCompound.putInt(TOTAL_ESSENCE_NBT, totalEssence);
        nbtTagCompound.putInt(WORKING_NBT, working);
    }

    public void readFromNBT(CompoundNBT nbtTagCompound) {
        remainingEssence = nbtTagCompound.getInt(STORED_ESSENCE_NBT);
        totalEssence = nbtTagCompound.getInt(TOTAL_ESSENCE_NBT);
        working = nbtTagCompound.getInt(WORKING_NBT);
    }

    public static final int WORKING_INDEX = 0;
    public static final int TOTAL_ESSENCE_INDEX = 1;
    public static final int REMAINING_ESSENCE_INDEX = 2;

    @Override
    public int get(int index) {
        validateIndex(index);
        switch (index) {
            case WORKING_INDEX: {
                return working;
            }

            case TOTAL_ESSENCE_INDEX: {
                return totalEssence;
            }

            case REMAINING_ESSENCE_INDEX: {
                return remainingEssence;
            }

            default:
            {
                return 0;
            }
        }
    }

    @Override
    public void set(int index, int value) {
        validateIndex(index);
        switch (index) {
            case WORKING_INDEX: {
                 working = value;
            }

            case TOTAL_ESSENCE_INDEX: {
                totalEssence = value;
            }

            case REMAINING_ESSENCE_INDEX: {
                remainingEssence = value;
            }
        }
    }

    @Override
    public int size() {
        return 3;
    }

    private void validateIndex(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException("Index out of bounds:"+index);
        }
    }
}
