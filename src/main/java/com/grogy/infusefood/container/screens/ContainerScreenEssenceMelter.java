package com.grogy.infusefood.container.screens;

import com.grogy.infusefood.Ref;
import com.grogy.infusefood.container.ContainerEssenceMelter;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.text.ITextComponent;

import java.awt.*;

public class ContainerScreenEssenceMelter extends ContainerScreen<ContainerEssenceMelter> {
    private ContainerEssenceMelter containerEssenceMelter;
    public ContainerScreenEssenceMelter(ContainerEssenceMelter containerEssenceMelter, PlayerInventory inv, ITextComponent titleIn) {
        super(containerEssenceMelter, inv, titleIn);

        this.containerEssenceMelter = containerEssenceMelter;

        // Set the width and height of the gui.  Should match the size of the texture!
        xSize = 176;
        ySize = 207;
    }

    final static  int PROGRESS_XPOS = 82;
    final static  int PROGRESS_YPOS = 15;
    final static  int PROGRESS_ICON_U = 176;   // texture position of flame icon [u,v]
    final static  int PROGRESS_ICON_V = 0;
    final static  int PROGRESS_WIDTH = 13;
    final static  int PROGRESS_HEIGHT = 14;

    final static  int FONT_Y_SPACING = 10;
    final static  int PLAYER_INV_LABEL_XPOS = ContainerEssenceMelter.PLAYER_INVENTORY_XPOS;
    final static  int PLAYER_INV_LABEL_YPOS = ContainerEssenceMelter.PLAYER_INVENTORY_YPOS - FONT_Y_SPACING;

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(Ref.Gui.ESSENCE_MELTER);

        // draw the background for this window
        int edgeSpacingX = (this.width - this.xSize) / 2;
        int edgeSpacingY = (this.height - this.ySize) / 2;
        this.blit(matrixStack, edgeSpacingX, edgeSpacingY, 0, 0, this.xSize, this.ySize);

        // draw the cook progress bar
        double cookProgress = containerEssenceMelter.fractionRemaining();
        int yProgress = (int)  (cookProgress * PROGRESS_HEIGHT);
        this.blit(matrixStack, guiLeft + PROGRESS_XPOS, guiTop + PROGRESS_YPOS + PROGRESS_HEIGHT - yProgress, PROGRESS_ICON_U, PROGRESS_ICON_V + PROGRESS_HEIGHT - yProgress,
                PROGRESS_WIDTH,yProgress);

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
    }

    @Override
    protected void renderHoveredTooltip(MatrixStack matrixStack, int x, int y) {
        super.renderHoveredTooltip(matrixStack, x, y);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int x, int y) {
        // draw the label for the top of the screen
        final int LABEL_XPOS = 5;
        final int LABEL_YPOS = 5;
        this.font.func_243248_b(matrixStack, this.title, LABEL_XPOS, LABEL_YPOS, Color.darkGray.getRGB());     ///    this.font.drawString

        // draw the label for the player inventory slots
        this.font.func_243248_b(matrixStack, this.playerInventory.getDisplayName(),                  ///    this.font.drawString
                PLAYER_INV_LABEL_XPOS, PLAYER_INV_LABEL_YPOS, Color.darkGray.getRGB());
    }


}
