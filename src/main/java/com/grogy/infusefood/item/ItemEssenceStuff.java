package com.grogy.infusefood.item;

import com.grogy.infusefood.Ref;
import com.grogy.infusefood.registry.BlockRegistry;
import com.grogy.infusefood.tile.TileEssenceNode;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;

public class ItemEssenceStuff extends Item {
    private final String IS_LINKING_NBT = "isLinkingNodes";
    private final String LINKING_NODE_NBT = "linkedNode";

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if(worldIn.isRemote) return super.onItemRightClick(worldIn, playerIn, handIn);
        RayTraceResult res = Minecraft.getInstance().objectMouseOver;
        if (res != null && res.getType() == RayTraceResult.Type.BLOCK) {
            CompoundNBT nbt;
            ItemStack itemstack = playerIn.getHeldItem(handIn);
            if (itemstack.hasTag())
            {
                nbt = itemstack.getTag();
            }
            else
            {
                nbt = new CompoundNBT();
            }
            BlockPos pos = new BlockPos(res.getHitVec());
            if(worldIn.getBlockState(pos).getBlock() == BlockRegistry.essenceNode) {
                assert nbt != null;
                if(nbt.getBoolean(IS_LINKING_NBT)) {
                    BlockPos second = BlockPos.fromLong(nbt.getLong(LINKING_NODE_NBT));
                    if(pos.distanceSq(second) > 16*16 || second == pos) {
                        nbt.putBoolean(IS_LINKING_NBT,false);
                        playerIn.sendStatusMessage(new StringTextComponent("Linking failed"),false);
                        itemstack.setTag(nbt);
                        return ActionResult.resultFail(itemstack);
                    }

                    TileEntity tile1 = worldIn.getTileEntity(second);
                    TileEntity tile2 = worldIn.getTileEntity(pos);

                    if(tile1 instanceof TileEssenceNode && tile2 instanceof TileEssenceNode) {
                        ((TileEssenceNode) tile1).unlinkConnections();
                        ((TileEssenceNode) tile2).unlinkConnections();

                        ((TileEssenceNode) tile1).linkWithOther(((TileEssenceNode) tile2));
                        playerIn.sendStatusMessage(new StringTextComponent("Linked successfully"),false);
                    }

                    nbt.putBoolean(IS_LINKING_NBT,false);
                    itemstack.setTag(nbt);
                    return ActionResult.resultSuccess(itemstack);
                } else {
                    nbt.putBoolean(IS_LINKING_NBT,true);
                    nbt.putLong(LINKING_NODE_NBT, pos.toLong());
                    playerIn.sendStatusMessage(new StringTextComponent("Linking " + pos.toString() ),false);
                    itemstack.setTag(nbt);
                    return ActionResult.resultSuccess(itemstack);
                }
            }
        }
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    public ItemEssenceStuff() {
        super(new Item.Properties().group(Ref.Inventory.CREATIVE_TAB) // the item will appear on the Miscellaneous tab in creative
        );
    }

    @Override
    public boolean updateItemStackNBT(CompoundNBT nbt) {
        return super.updateItemStackNBT(nbt);
    }
}
